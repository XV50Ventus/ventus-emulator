﻿namespace OpenNos.Domain
{
    public enum SkillType
    {
        CharacterPassiveSkill = 0,
        CharacterSKill = 1,
        CharacterSkillUpgraded = 2,
        Emote = 3,
        MonsterSkill = 4,
        PartnerSpSkill = 5
    }
}