﻿namespace OpenNos.Domain
{
    public enum HairStyleType : byte
    {
        HairStyleA = 0,
        HairStyleB = 1,
        HairStyleC = 2,
        HairStyleD = 3,
        NoHair = 4,
        Hair6 = 10,
        Hair7 = 11,
        Hair8 = 12
    }
}