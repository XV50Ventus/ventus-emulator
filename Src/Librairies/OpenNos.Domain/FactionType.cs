﻿namespace OpenNos.Domain
{
    public enum FactionType : byte
    {
        None = 0,
        Angel = 1,
        Demon = 2
    }
}