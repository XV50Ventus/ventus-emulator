﻿namespace OpenNos.Domain
{
    public enum RarifyMode
    {
        Normal,
        Reduced,
        Free,
        Drop,
        Success,
        HeroEquipmentDowngrade
    }
}