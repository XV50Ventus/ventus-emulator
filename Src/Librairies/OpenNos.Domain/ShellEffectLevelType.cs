﻿namespace OpenNos.Domain
{
    public enum ShellEffectLevelType : byte
    {
        CNormal = 1,
        BNormal = 2,
        ANormal = 3,
        SNormal = 4,
        CBonus = 5,
        BBonus = 6,
        ABonus = 7,
        SBonus = 8,
        CPVP = 9,
        BPVP = 10,
        APVP = 11,
        SPVP = 12
    }
}