﻿namespace OpenNos.Domain
{
    public enum AuthorityType : short
    {
        Closed = -3,
        Banned = -2,
        Unconfirmed = -1,
        User = 0,
        GS = 1,
        VIP = 2,
        GM = 3,
        Administrator = 4
    }
}