﻿namespace OpenNos.Domain
{
    public enum EntityType : byte
    {
        Player = 0,
        Mate = 1,
        Monster = 2,
        Npc = 3
    }
}