﻿namespace OpenNos.Domain
{
    public enum GroupType : byte
    {
        Group = 3,
        TalentArena = 6,
        Team = 8,
        BigTeam = 15,
        GiantTeam = 40,
        RainbowBattleBlue = 15,
        RainbowBattleRed = 15
    }
}