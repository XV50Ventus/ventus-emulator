﻿namespace OpenNos.Domain
{
    public enum FamilyAuthority : byte
    {
        Head = 0,
        Familydeputy = 1,
        Familykeeper = 2,
        Member = 3
    }
}