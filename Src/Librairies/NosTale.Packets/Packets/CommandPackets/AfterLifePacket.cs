﻿using OpenNos.Core;
using OpenNos.Domain;

namespace NosTale.Packets.Packets.CommandPackets
{
    [PacketHeader("$AfterLife", PassNonParseablePacket = true, Authorities = new[] { AuthorityType.Administrator })]
    public class AfterLifePacket : PacketDefinition
    {

    }
}