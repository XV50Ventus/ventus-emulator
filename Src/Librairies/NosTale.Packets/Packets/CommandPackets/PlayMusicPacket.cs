﻿using OpenNos.Core;
using OpenNos.Domain;

namespace NosTale.Packets.Packets.CommandPackets
{
    [PacketHeader("$PlayMusic", PassNonParseablePacket = true, Authorities = new[] { AuthorityType.User })]
    public class PlayMusicPacket : PacketDefinition
    {

        [PacketIndex(0)] public string Song { get; set; }
    }
}