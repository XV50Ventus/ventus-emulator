﻿using OpenNos.Core;
using OpenNos.Domain;

namespace NosTale.Packets.Packets.CommandPackets
{
    [PacketHeader("$Msg", PassNonParseablePacket = true, Authorities = new[] { AuthorityType.VIP})]
    public class MsgPacket : PacketDefinition
    {
        [PacketIndex(0)]
        public byte Type { get; set; }

        [PacketIndex(1)]
        public string Message { get; set; }

        public static string ReturnHelp() => "$Msg <Type> <Message>";

    }
}