﻿using NosTale.Configuration.Configuration.Server;

namespace NosTale.Configuration
{
    public class JsonGameConfiguration
    {
        // Here some Config ↓
        public ServerConfiguration Server { get; set; } = new ServerConfiguration
        {
            //Auth
            AuthentificationServiceAuthKey = "ventus2021",
            MasterAuthKey = "ventus2021",
            LogKey = "LogAuth147", // this isn't being used as far as i know

            // Config ip/port
            LogerPort = 6970,
            IPAddress = "82.165.111.190",
            MasterIP = "127.0.0.1",
            Act4Port = 5100,
            LoginPort = 4000,
            MasterPort = 4545,
            WorldPort = 1337,

            // Config Srv
            ServerGroupS1 = "S1-Ignis",
            Language = "uk",
            SessionLimit = 150,
            LagMode = false,
            SceneOnCreate = false,
            UseOldCrypto = false,
            WorldInformation = true
        };

        public RateConfiguration Rate { get; set; } = new RateConfiguration
        {
            CylloanPercentRate = 30,
            GlacernonPercentRatePvm = 5,
            GlacernonPercentRatePvp = 1,
            RateXP = 20,
            PartnerSpXp = 5,
            QuestDropRate = 1,
            RateDrop = 5,
            RateFairyXP = 20,
            RateGold = 7,
            RateGoldDrop = 5,
            RateHeroicXP = 999,
            RateReputation = 1
        };

        public EventConfiguration Event { get; set; } = new EventConfiguration
        {
            ChristmasEvent = false,
            HalloweenEvent = false
        };

        public MaxConfiguration Max { get; set; } = new MaxConfiguration
        {
            HeroicStartLevel = 88,
            MaxGold = int.MaxValue,
            MaxGoldBank = 200000000000,
            MaxHeroLevel = 60,
            MaxJobLevel = 80,
            MaxLevel = 250,
            MaxSPLevel = 99,
            MaxUpgrade = 10
        };
    }
}