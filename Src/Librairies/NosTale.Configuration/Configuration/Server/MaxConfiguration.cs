﻿namespace NosTale.Configuration.Configuration.Server
{
    public struct MaxConfiguration
    {
        #region Properties

        public byte HeroicStartLevel { get; set; }

        public long MaxGold { get; set; }

        public byte MaxHeroLevel { get; set; }

        public byte MaxJobLevel { get; set; }

        public byte MaxLevel { get; set; }

        public byte MaxSPLevel { get; set; }

        public byte MaxUpgrade { get; set; }

        public long MaxGoldBank { get; set; }

        #endregion
    }
}