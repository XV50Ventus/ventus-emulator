﻿namespace NosTale.Configuration.Configuration.Server
{
    public struct EventConfiguration
    {
        #region Properties

        public bool ChristmasEvent { get; set; }

        public bool HalloweenEvent { get; set; }

        #endregion
    }
}