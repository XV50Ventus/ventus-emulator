﻿namespace NosTale.Configuration.Configuration.Item
{
    public struct InventoryConfiguration
    {
        #region Properties

        public short MaxItemPerSlot { get; set; }

        #endregion
    }
}