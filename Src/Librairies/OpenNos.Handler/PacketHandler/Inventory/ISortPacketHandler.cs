﻿using NosTale.Packets.Packets.ClientPackets;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using System;

namespace OpenNos.Handler.PacketHandler.Inventory
{
    public class ISortPacketHandler : IPacketHandler
    {

        public ISortPacketHandler(ClientSession session)
        {
            Session = session;
        }

        public ClientSession Session { get; }

        public void Sort(ISortPacket iSortPacket)
        {
            if (iSortPacket?.InventoryType.HasValue == true)
            {
                var time = Session.Character.LastSort.AddSeconds(5);

                if (DateTime.Now <= time)
                {
                    Session.SendPacket(Session.Character.GenerateSay("The Sort System is in cooldown. Please wait up to 5 seconds!", 11));
                    return;
                }

                if (iSortPacket.InventoryType == InventoryType.Equipment || iSortPacket.InventoryType == InventoryType.Etc || iSortPacket.InventoryType == InventoryType.Main)

                {
                    Session.Character.Inventory.Reorder(Session, iSortPacket.InventoryType.Value);
                }

            }
        }
    }
}