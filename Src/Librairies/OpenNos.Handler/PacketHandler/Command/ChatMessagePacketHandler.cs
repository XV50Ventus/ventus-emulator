﻿using System;
using NosTale.Extension.Extension.Command;
using NosTale.Packets.Packets.CommandPackets;
using OpenNos.Core;
using OpenNos.GameObject;
using OpenNos.GameObject.Extension;
using OpenNos.Domain;

namespace OpenNos.Handler.PacketHandler.Command
{
    public class ChatMessagePacketHandler : IPacketHandler
    {
        #region Instantiation

        public ChatMessagePacketHandler(ClientSession session) => Session = session;

        #endregion

        #region Properties

        public ClientSession Session { get; }

        #endregion

        #region Methods

        public void Say(ChatMessagePacket chatMessagePacket)
        {
            Session.CurrentMapInstance.Broadcast($"say {Session.Character.CharacterId}, 1 {chatMessagePacket.Type} {chatMessagePacket.Message}", ReceiverType.All);
        }

        #endregion
    }
}