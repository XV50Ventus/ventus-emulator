﻿using NosTale.Packets.Packets.CommandPackets;
using OpenNos.Core;
using OpenNos.GameObject;
using OpenNos.GameObject.Extension;

namespace OpenNos.Handler.PacketHandler.Command
{
    public class MsgPacketHandler : IPacketHandler
    {
        public MsgPacketHandler(ClientSession session) => Session = session;

        public ClientSession Session { get; }

        public void SendMessage(MsgPacket msgPacket)
        {
           
            Session.CurrentMapInstance?.Broadcast($"msg {msgPacket.Type} {msgPacket.Message}");

        }
    }
}