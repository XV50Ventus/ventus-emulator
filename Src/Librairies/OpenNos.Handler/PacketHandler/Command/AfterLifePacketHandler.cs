﻿using NosTale.Packets.Packets.CommandPackets;
using OpenNos.Core;
using OpenNos.GameObject;
using OpenNos.GameObject.Extension;
using OpenNos.GameObject.Networking;

namespace OpenNos.Handler.PacketHandler.Command
{
    public class AfterLifePacketHandler : IPacketHandler
    {
        public AfterLifePacketHandler(ClientSession session)
        {
            Session = session;
        }
        public ClientSession Session { get; }

        public void DlgTest(AfterLifePacket afterLifePacket)
        {
            Session.SendPacket($"msg 4 Value: {Session.Character.UltraClass}");
        }
    }
}