﻿using NosTale.Packets.Packets.CommandPackets;
using OpenNos.Core;
using OpenNos.GameObject;
using OpenNos.GameObject.Extension;

namespace OpenNos.Handler.PacketHandler.Command
{
    public class PositionHandler : IPacketHandler
    {
        #region Instantiation

        public PositionHandler(ClientSession session) => Session = session;

        #endregion

        #region Properties

        public ClientSession Session { get; }

        #endregion

        #region Methods

        public void Position(PositionPacket positionPacket)
        {
            Session.AddLogsCmd(positionPacket);
            
            Session.AddLogsCmd(positionPacket);
            Session.SendPacket(Session.Character.GenerateSay($"MapID: {Session.Character.MapInstance.Map.MapId}", 12));
            Session.SendPacket(Session.Character.GenerateSay($"MapX: {Session.Character.PositionX}", 12));
            Session.SendPacket(Session.Character.GenerateSay($"MapY: {Session.Character.PositionY}", 12));
            Session.SendPacket(Session.Character.GenerateSay($"MapDir: {Session.Character.Direction}", 11));
            Session.SendPacket(Session.Character.GenerateSay($"MapCell: {Session.CurrentMapInstance.Map.JaggedGrid[Session.Character.PositionX][Session.Character.PositionY]?.Value}", 11));
        }

        #endregion
    }
}