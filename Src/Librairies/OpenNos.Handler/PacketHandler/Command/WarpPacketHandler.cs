﻿using NosTale.Extension.Extension.Command;
using NosTale.Packets.Packets.CommandPackets;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Extension;
using OpenNos.GameObject.Networking;
using System;

namespace OpenNos.Handler.PacketHandler.Command
{
    public class WarpPacketHandler : IPacketHandler
    {
        public WarpPacketHandler(ClientSession session) => Session = session;

        public ClientSession Session { get; }


        public void Warp(WarpPacket warpPacket)
        {

            #region Checks
            var time = Session.Character.LastWarp.AddSeconds(30);
            if (DateTime.Now <= time) 
            {
                Session.SendPacket(Session.Character.GenerateSay("The Warp System is on Cooldown. You have to wait 30 Seconds!", 11));
                return;
            }

            if (ServerManager.Instance.ChannelId == 51)
            {
                Session.SendPacket(Session.Character.GenerateSay("The Warp System won't work in the Glacernon!", 11));
                return;
            }

            if (Session.Character.LastSkillUse.AddSeconds(20) > DateTime.Now || Session.Character.LastDefence.AddSeconds(20) > DateTime.Now)
            {
                Session.SendPacket(Session.Character.GenerateSay("The Warp System won't work when in Battle!", 11));
                return;
            }

            #endregion

            string notEnoughGold = "msg 3 Not enough Gold!";

            switch (warpPacket.Destination)
            {
                case "Help":
                    Session.SendPacket(Session.Character.GenerateSay("=== Warp System ===", 10));
                    Session.SendPacket(Session.Character.GenerateSay("Version: 1.0.0.0 Beta", 11));
                    Session.SendPacket(Session.Character.GenerateSay("If you have any problems, make sure to let the Team know", 11));
                    Session.SendPacket(Session.Character.GenerateSay("===================", 10));
                    Session.SendPacket(Session.Character.GenerateSay("[Warp]: NosVille | 5.000 Gold", 12));
                    Session.SendPacket(Session.Character.GenerateSay("[Warp]: Cylloan | 20.000 Gold", 12));
                    Session.SendPacket(Session.Character.GenerateSay("[Warp]: Alveus | 5.000 Gold", 12));
                    Session.SendPacket(Session.Character.GenerateSay("[Warp]: Raid1 (Cuby) | 5.000 Gold", 12));
                    Session.SendPacket(Session.Character.GenerateSay("[Warp]: Raid2 (Ginseng) | 15.000 Gold", 12));
                    Session.SendPacket(Session.Character.GenerateSay("[Warp]: Raid3 (Slade) | 15.000 Gold", 12));
                    Session.SendPacket(Session.Character.GenerateSay("[Warp]: Raid4 (Spider) | 15.000 Gold", 12));
                    break;

                case "NosVille":
                    if (Session.Character.Gold > 5000)
                    {
                        Session.SendPacket(notEnoughGold);
                        return;
                    }
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 1, 80, 116);
                    Session.Character.Gold -= 5000;
                    break;
            }
        }
    }
}