﻿using NosTale.Packets.Packets.CommandPackets;
using OpenNos.Core;
using OpenNos.GameObject;
using OpenNos.GameObject.Extension;

namespace OpenNos.Handler.PacketHandler.Command
{
    public class EnableAutoLootPacketHandler : IPacketHandler
    {
        public EnableAutoLootPacketHandler (ClientSession session) => Session = session;

        public ClientSession Session { get; }

        public void AutoLoot(EnableAutoLootPacket enableAutoLootPacket)
        {
            if (Session.Character.AutoLoot == 0)
            {
                Session.Character.AutoLoot = 1;
                Session.SendPacket("msg 4 The Autoloot Plugin has been enabled!");
                Session.SendPacket(Session.Character.GenerateSay("=============================", 12));
                Session.SendPacket(Session.Character.GenerateSay("ATTENTION!", 11));
                Session.SendPacket(Session.Character.GenerateSay("While the Autoloot Plugin is enabled, you cannot collect Quest Items!", 10));
                Session.SendPacket(Session.Character.GenerateSay("=============================", 12));
                return;
            }

            if (Session.Character.AutoLoot == 1)
            {
                Session.Character.AutoLoot = 0;
                Session.SendPacket("msg 4 The Autoloot Plugin has been disabled!");
                return;
            }
        }
    }
}