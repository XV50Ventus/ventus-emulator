﻿using NosTale.Packets.Packets.CommandPackets;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Extension;

namespace OpenNos.Handler.PacketHandler.Command
{
    public class PlayMusicPacketHandler : IPacketHandler
    {
        public PlayMusicPacketHandler(ClientSession session)
        {
            Session = session;
        }

        public ClientSession Session { get; }

        public void Play(PlayMusicPacket playMusicPacket)
        {
            if (playMusicPacket.Song == "Help")
            {
                Session.SendPacket(Session.Character.GenerateSay("Available Songs: $PlayMusic Lofi", 12));
            }
           if (playMusicPacket.Song == "Lofi")
            {
                Session.SendPacket(Session.Character.GenerateSay("[Musicbot]: Now playing: 'Lofi Hip Hop Remix' - Enjoy!", 11));
                Session.SendPacket("bgm 1");
            }
        }
    }
}