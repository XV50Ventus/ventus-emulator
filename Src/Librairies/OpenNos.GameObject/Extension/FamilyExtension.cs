﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenNos.Domain;

namespace OpenNos.GameObject.Extension
{
    public static class FamilyExtension
    {
        public static string GetFamilyNameType(this Character e)
        {
            var thisRank = e.FamilyCharacter.Authority;

            return thisRank == FamilyAuthority.Member ? "918" :
                thisRank == FamilyAuthority.Familydeputy ? "916" :
                thisRank == FamilyAuthority.Familykeeper ? "917" :
                thisRank == FamilyAuthority.Head ? "915" : "-1 -";
        }
    }
}