﻿namespace ChickenAPI.Plugins
{
    public enum PluginEnableTime
    {
        PreContainerBuild,
        PostContainerBuild
    }
}