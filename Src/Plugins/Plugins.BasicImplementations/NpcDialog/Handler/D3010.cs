﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject._NpcDialog;
using OpenNos.GameObject._NpcDialog.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace Plugins.BasicImplementations.NpcDialog.Handler
{
    public class D3010 : INpcDialogAsyncHandler
    {
        //TODO: Add Checks
        //TODO: Add Coordinates

        #region Methods
        public long HandledId => 3010;

        public async Task Execute(ClientSession Session, NpcDialogEvent packet)
        {
            var npc = packet.Npc;

            List<ClientSession> sessions = new List<ClientSession>();
            if (Session.Character.Group != null)
            {
                sessions = Session.Character.Group.Sessions.Where(s => s.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance);
            }
            else
            {
                sessions.Add(Session);
            }
            List<Tuple<MapInstance, byte>> maps = new List<Tuple<MapInstance, byte>>();
            MapInstance map = null;
            byte instanceHeroLevel = 1;
            byte instanceLevel = 1;
            #endregion

        #region NRun
            switch (packet.Type)
            {
                #region Leveling Instances

                #region 1-20
                case 0:
                    map = ServerManager.GenerateMapInstance(30009, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 20;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 20-30
                case 1:
                    map = ServerManager.GenerateMapInstance(30010, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 30;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 30-40
                case 2:
                    map = ServerManager.GenerateMapInstance(30011, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 40;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 40-50
                case 3:
                    map = ServerManager.GenerateMapInstance(30012, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 50;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 50-60
                case 4:
                    map = ServerManager.GenerateMapInstance(30013, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 60;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 60-70
                case 5:
                    map = ServerManager.GenerateMapInstance(30014, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 70;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 70-80
                case 6:
                    map = ServerManager.GenerateMapInstance(30015, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 80;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 80-90
                case 7:
                    map = ServerManager.GenerateMapInstance(30016, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 90;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region 90-99
                case 8:
                    map = ServerManager.GenerateMapInstance(30017, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 99;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 14, 42);
                        }
                    }
                    break;
                #endregion

                #region Land of Death
                case 9:
                    map = ServerManager.GenerateMapInstance(150, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceHeroLevel = 20;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 150, 150);
                        }
                    }
                    break;
                #endregion

                #region Eastern Path
                case 10:
                    map = ServerManager.GenerateMapInstance(206, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceHeroLevel = 30;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 22, 22);
                        }
                    }
                    break;
                #endregion

                #endregion
                    
                #region Farming Instances

                #region Gold (50-70)
                case 11:
                    map = ServerManager.GenerateMapInstance(30020, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 70;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 22, 37);
                        }
                    }
                    break;
                #endregion

                #region SP-Upgrade Materials (70-90)
                case 12:
                    map = ServerManager.GenerateMapInstance(30021, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 90;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 22, 37);
                        }
                    }
                    break;
                #endregion

                #region EQ-Upgrade Materials (70-90)
                case 13:
                    map = ServerManager.GenerateMapInstance(30022, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 90;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 22, 37);
                        }
                    }
                    break;
                #endregion

                #region Perfection Stones (90-99)
                case 14:
                    map = ServerManager.GenerateMapInstance(30023, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 90;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 22, 37);
                        }
                    }
                    break;
                #endregion

                #endregion

                #region Equipment Instances

                #region Level 92-93
                case 15:
                    map = ServerManager.GenerateMapInstance(30024, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 99;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            s.SendPacket("msg 4 Coming soon!");
                            //ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 39, 67);
                        }
                    }
                    break;
                #endregion

                #region Level 95-96
                case 16:
                    map = ServerManager.GenerateMapInstance(30025, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 99;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            s.SendPacket("msg 4 Coming soon!");
                            //ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 39, 67);
                        }
                    }
                    break;
                #endregion

                #region Herolevel 25-28
                case 17:
                    map = ServerManager.GenerateMapInstance(30026, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 99;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            s.SendPacket("msg 4 Coming soon!");
                            //ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 39, 67);
                        }
                    }
                    break;
                #endregion

                #region Herolevel 45-48
                case 18:
                    map = ServerManager.GenerateMapInstance(30027, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 99;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            s.SendPacket("msg 4 Coming soon!");
                            //ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 39, 67);
                        }
                    }
                    break;
                #endregion

                #region Herolevel 58-60
                case 19:
                    map = ServerManager.GenerateMapInstance(30028, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 99;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            s.SendPacket("msg 4 Coming soon!");
                            //ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 39, 67);
                        }
                    }
                    break;
                #endregion

                #region Ultraclass Grade 1 and 2
                case 20:
                    map = ServerManager.GenerateMapInstance(30029, MapInstanceType.TimeSpaceInstance, new InstanceBag());
                    instanceLevel = 99;
                    maps.Add(new Tuple<MapInstance, byte>(map, instanceLevel));

                    if (map != null)
                    {
                        foreach (ClientSession s in sessions)
                        {
                            s.SendPacket("msg 4 Coming soon!");
                            //ServerManager.Instance.ChangeMapInstance(s.Character.CharacterId, map.MapInstanceId, 39, 67);
                        }
                    }
                    break;
                    #endregion

                #endregion
            }
        }
        #endregion
    }
}