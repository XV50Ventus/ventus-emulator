﻿using System;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject._NpcDialog;
using OpenNos.GameObject._NpcDialog.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace Plugins.BasicImplementations.NpcDialog.Handler
{
    public class D3002 : INpcDialogAsyncHandler
    {
        public long HandledId => 3002;

        public async Task Execute(ClientSession Session, NpcDialogEvent packet)
        {
            var npc = packet.Npc;

            switch (packet.Type)
            {

                #region Magician's Path

                #region Fiery Magician
                case 0:
                    if (Session.Character.Level < 99)
                    {
                        Session.SendPacket($"msg 4 You need to be at least Level 99. Your Level: {Session.Character.Level}");
                        return;
                    }
                    if (Session.Character.UltraClass == 2)
                    {
                        return;
                    }
                    if (Session.Character.UltraClass == 3)
                    {
                        return;
                    }
                    if (Session.Character.HeroLevel < 60)
                    {
                        Session.SendPacket($"msg 4 You need to be at least Hero Level 60. Your Hero Level: {Session.Character.HeroLevel}");
                        return;
                    }
                    if (Session.Character.UltraClass > 0)
                    {
                        Session.SendPacket($"msg 4 You already chose a path!");
                        return;
                    }
                    if (Session.Character.Class == ClassType.Magician && Session.Character.UltraClass == 0)
                    {
                        Session.SendPacket($"msg 4 Congratulations, {Session.Character.Name}. You are now a Fiery Magician!");
                        //Set to Fiery Magician
                        Session.Character.UltraClass = 1;
                    }
                    break;
                #endregion

                #region Frozen Magician
                case 1:
                    if (Session.Character.Level < 99)
                    {
                        Session.SendPacket($"You need to be at least Level 99. Your Level: {Session.Character.Level}");
                        return;
                    }

                    if (Session.Character.HeroLevel < 60)
                    {
                        Session.SendPacket($"You need to be at least Hero Level 60. Your Hero Level: {Session.Character.HeroLevel}");
                        return;
                    }
                    if (Session.Character.UltraClass == 1)
                    {
                        return;
                    }
                    if (Session.Character.UltraClass == 3)
                    {
                        return;
                    }
                    if (Session.Character.UltraClass > 0)
                    {
                        Session.SendPacket($"msg 4 You already chose a path!");
                        return;
                    }
                    if (Session.Character.Class == ClassType.Magician)
                    {
                        Session.SendPacket($"msg 4 Congratulations, {Session.Character.Name}. You are now a Frozen Magician!");
                        //Set to Frozen Magician
                        Session.Character.UltraClass += 2;
                    }
                    break;
                #endregion

                #region Ultimate Sorcerer
                case 2:
                    if (Session.Character.Level < 99)
                    {
                        Session.SendPacket($"You need to be at least Level 99. Your Level: {Session.Character.Level}");
                        return;
                    }
                    if (Session.Character.UltraClass == 1)
                    {
                        return;
                    }
                    if (Session.Character.UltraClass == 2)
                    {
                        return;
                    }
                    if (Session.Character.HeroLevel < 60)
                    {
                        Session.SendPacket($"You need to be at least Hero Level 60. Your Hero Level: {Session.Character.HeroLevel}");
                        return;
                    }
                    if (Session.Character.UltraClass > 0)
                    {
                        Session.SendPacket($"msg 4 You already chose a path!");
                        return;
                    }
                    if (Session.Character.Class == ClassType.Magician)
                    {
                        Session.SendPacket($"msg 4 Congratulations, {Session.Character.Name}. You are now a Ultimate Sorcerer!");
                        //Set to Ultimate Sorcerer
                        Session.Character.UltraClass += 3;
                    }
                    break;
                #endregion 

                #endregion

                #region Archer's Path
                case 3:
                    break;

                case 4:
                    break;

                case 5:
                    break;

                case 6:
                    break;

                case 7:
                    break;
                #endregion

                #region Swordsman Path
                case 8:
                    break;

                case 9:
                    break;

                case 10:
                    break;

                case 11:
                    break;

                case 12:
                    break;

                case 13:
                    break;
                    #endregion 
            }
        }
    }
}