﻿using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.GameObject;
using OpenNos.GameObject._NpcDialog;
using OpenNos.GameObject._NpcDialog.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace Plugins.BasicImplementations.NpcDialog.Handler
{
    public class D2002 : INpcDialogAsyncHandler
    {
        public long HandledId => 2002;

        public async Task Execute(ClientSession Session, NpcDialogEvent packet)
        {
           var npc = packet.Npc;
            switch (npc.NpcVNum)
            {
                //Pijama
                case 935:
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 30001, 7, 9);
                    break;

                case 936:
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 30002, 7, 9);
                    break;

                case 937:
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 30003, 7, 9);
                    break;

                case 952:
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 30004, 7, 9);
                    break;

                case 953:
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 30005, 7, 9);
                    break;
            }
        }
    }
}