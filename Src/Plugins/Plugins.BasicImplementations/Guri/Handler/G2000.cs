﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject._Guri;
using OpenNos.GameObject._Guri.Event;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace Plugins.BasicImplementations.Guri.Handler
{
    public class G2000 : IGuriHandler
    {
        public long GuriEffectId => 2000;

        public async Task ExecuteAsync(ClientSession Session, GuriEvent e)
        {
            var familyHead = ServerManager.Instance.GetCharacterById(e.User);

            if (familyHead == null)
            {
                Session.SendPacket("msg 4 Something went wrong. Seems like the Familyhead went offline!");
                return;
            }
            if (e.Argument == 0)
            {
                ServerManager.Instance.ChangeMap(Session.Character.CharacterId, familyHead.MapId, familyHead.MapX, familyHead.MapY);
            }
            else
            {
                familyHead.Session.SendPacket($"msg 4 {Session.Character.Name} declined your summoning.");
            }
        }
    }
}