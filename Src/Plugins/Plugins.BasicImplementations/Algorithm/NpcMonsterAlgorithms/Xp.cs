﻿using OpenNos.Domain;

namespace Plugins.BasicImplementations.Algorithm.NpcMonsterAlgorithms
{
    public class Xp : IMonsterRaceStatAlgorithm
    {
        private const int MAX_LEVEL = 256;
        private int[] _stats;

        public void Initialize()
        {
            _stats = new int[MAX_LEVEL];

            for (var i = 0; i < MAX_LEVEL; i++) _stats[i] = i < 18 ? i * 60 : i * 70;
        }

        public int GetStat(NpcMonsterRaceType type, byte level, bool isMonster)
        {
            return _stats[level];
        }
    }
}