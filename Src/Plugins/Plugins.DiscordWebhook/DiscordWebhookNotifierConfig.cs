﻿using System.Collections.Generic;
using Discord;
using OpenNos.GameObject._gameEvent;

namespace Plugins.DiscordWebhook
{
     public class DiscordWebhookNotifierConfig
    {
        public Dictionary<NotifiableEventType, string> Messages = new Dictionary<NotifiableEventType, string>
        {
            {
                NotifiableEventType.ACT4_RAID_FIRE_STARTED_ANGEL, @"/!\
__**{0}**__ raid has started for __**{1}**__ !
/!\"
            },
            {
                NotifiableEventType.ACT4_RAID_WATER_STARTED_DEMON, @"/!\
__**{0}**__ raid has started for __**{1}**__ !
/!\"
            },
            { NotifiableEventType.FAMILY_X_HAS_BEEN_CREATED_BY_Y, "Family __**{0}**__ has been created  by __**{1}**__!" },
        };

        public Dictionary<NotifiableEventType, string> Thumbnails = new Dictionary<NotifiableEventType, string>
        {
            { NotifiableEventType.ACT4_RAID_FIRE_STARTED_ANGEL, "https://cdn.discordapp.com/attachments/605815407093481472/607209589330673694/alewe.png" },
            { NotifiableEventType.ACT4_RAID_WATER_STARTED_DEMON, "https://cdn.discordapp.com/attachments/605815407093481472/607209587925450788/dlewe1.png" },
            { NotifiableEventType.FAMILY_X_HAS_BEEN_CREATED_BY_Y, "https://i.ytimg.com/vi/5sRaVQuhYY4/maxresdefault.jpg" }
        };

        public Dictionary<NotifiableEventType, string> Images = new Dictionary<NotifiableEventType, string>
        {
            { NotifiableEventType.ACT4_RAID_FIRE_STARTED_ANGEL, "https://cdn.discordapp.com/attachments/605815407093481472/607205615143485441/unknown.png" },
            { NotifiableEventType.ACT4_RAID_WATER_STARTED_DEMON, "https://cdn.discordapp.com/attachments/605815407093481472/607212101622038528/calvinas.png" },
            { NotifiableEventType.FAMILY_X_HAS_BEEN_CREATED_BY_Y, "https://i.ytimg.com/vi/5sRaVQuhYY4/maxresdefault.jpg" }
        };
        
        public Dictionary<NotifiableEventType, string> Title = new Dictionary<NotifiableEventType, string>
        {
            { NotifiableEventType.ACT4_RAID_FIRE_STARTED_ANGEL, "ACT 4 RAID Started" },
            { NotifiableEventType.ACT4_RAID_WATER_STARTED_DEMON, "ACT 4 RAID Started" },
            { NotifiableEventType.FAMILY_X_HAS_BEEN_CREATED_BY_Y, "A Family As Been Created" }
        };

        public string IconUrl { get; set; } = "https://cdn.discordapp.com/attachments/605815407093481472/607205615143485441/unknown.png";

        public Dictionary<NotifiableEventType, Color> Colors { get; set; } = new Dictionary<NotifiableEventType, Color>
        {
            { NotifiableEventType.ACT4_RAID_FIRE_STARTED_ANGEL, new Color(0xdd5522) },
            { NotifiableEventType.ACT4_RAID_FIRE_STARTED_DEMON, new Color(0xdd5522) },
            { NotifiableEventType.ACT4_RAID_WATER_STARTED_ANGEL, new Color(0x33aadd) },
            { NotifiableEventType.ACT4_RAID_WATER_STARTED_DEMON, new Color(0x33aadd) },
            { NotifiableEventType.FAMILY_X_HAS_BEEN_CREATED_BY_Y, new Color(0x33aadd) },
        };
    }
}