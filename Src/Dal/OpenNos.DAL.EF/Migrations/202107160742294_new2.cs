﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CharacterTitle",
                c => new
                    {
                        CharacterTitleId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        TitleVnum = c.Long(nullable: false),
                        Stat = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.CharacterTitleId)
                .ForeignKey("dbo.Character", t => t.CharacterId)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.CharacterVisitedMaps",
                c => new
                    {
                        CharacterVisitedMapId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        MapId = c.Int(nullable: false),
                        MapX = c.Int(nullable: false),
                        MapY = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CharacterVisitedMapId)
                .ForeignKey("dbo.Character", t => t.CharacterId, cascadeDelete: true)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.BotAuthority",
                c => new
                    {
                        DiscordId = c.Long(nullable: false),
                        Authority = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DiscordId);
            
            CreateTable(
                "dbo.BoxItem",
                c => new
                    {
                        BoxItemId = c.Long(nullable: false, identity: true),
                        ItemGeneratedAmount = c.Short(nullable: false),
                        ItemGeneratedDesign = c.Short(nullable: false),
                        ItemGeneratedRare = c.Byte(nullable: false),
                        ItemGeneratedUpgrade = c.Byte(nullable: false),
                        ItemGeneratedVNum = c.Short(nullable: false),
                        OriginalItemDesign = c.Short(nullable: false),
                        OriginalItemVNum = c.Short(nullable: false),
                        Probability = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.BoxItemId);
            
            CreateTable(
                "dbo.ChatLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        CharacterId = c.Int(nullable: false),
                        CharacterName = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        Message = c.String(),
                        TargetCharacterId = c.Int(),
                        TargetCharacterName = c.String(),
                        Type = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LogCommands",
                c => new
                    {
                        CommandId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(),
                        Command = c.String(),
                        Data = c.String(),
                        IpAddress = c.String(maxLength: 255),
                        Name = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CommandId)
                .ForeignKey("dbo.Character", t => t.CharacterId)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.RuneEffect",
                c => new
                    {
                        RuneEffectId = c.Int(nullable: false, identity: true),
                        EquipmentSerialId = c.Guid(nullable: false),
                        Type = c.Byte(nullable: false),
                        SubType = c.Byte(nullable: false),
                        FirstData = c.Int(nullable: false),
                        SecondData = c.Int(nullable: false),
                        ThirdData = c.Int(nullable: false),
                        IsPower = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.RuneEffectId);
            
            AddColumn("dbo.Account", "GoldBank", c => c.Long(nullable: false));
            AddColumn("dbo.Character", "MaxPartnerCount", c => c.Byte(nullable: false));
            AddColumn("dbo.Character", "ArenaKill", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "ArenaDeath", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "IsChangeName", c => c.Boolean(nullable: false));
            AddColumn("dbo.Character", "MobKillCounter", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "UnlockedHLevel", c => c.Byte(nullable: false));
            AddColumn("dbo.Character", "HideHat", c => c.Boolean(nullable: false));
            AddColumn("dbo.Character", "UiBlocked", c => c.Boolean(nullable: false));
            AddColumn("dbo.Character", "UltraClass", c => c.Byte(nullable: false));
            AddColumn("dbo.Character", "AutoLoot", c => c.Byte(nullable: false));
            AddColumn("dbo.ItemInstance", "HasSkin", c => c.Boolean());
            AddColumn("dbo.ItemInstance", "IsBreaked", c => c.Boolean(nullable: false));
            AddColumn("dbo.ItemInstance", "RuneAmount", c => c.Byte(nullable: false));
            AddColumn("dbo.Item", "MorphSp", c => c.Short(nullable: false));
            AddColumn("dbo.NpcMonster", "EvolvePet", c => c.Short(nullable: false));
            AddColumn("dbo.MapNpc", "Score", c => c.Byte(nullable: false));
            AddColumn("dbo.CharacterSkill", "IsTattoo", c => c.Boolean(nullable: false));
            AddColumn("dbo.CharacterSkill", "TattooLevel", c => c.Byte(nullable: false));
            AddColumn("dbo.Portal", "RequiredItem", c => c.Short(nullable: false));
            AddColumn("dbo.Portal", "LevelRequired", c => c.Short(nullable: false));
            AddColumn("dbo.Portal", "HeroLevelRequired", c => c.Short(nullable: false));
            AddColumn("dbo.Portal", "NomeOggetto", c => c.String());
            AddColumn("dbo.Portal", "RequiredClass", c => c.Byte());
            AddColumn("dbo.ScriptedInstance", "QuestTimeSpaceId", c => c.Int(nullable: false));
            AddColumn("dbo.Quest", "CanBeDoneOnlyOnce", c => c.Boolean(nullable: false));
            AddColumn("dbo.ShellEffect", "IsRune", c => c.Boolean(nullable: false));
            AddColumn("dbo.ShellEffect", "Type", c => c.Short(nullable: false));
            AddColumn("dbo.ShellEffect", "Upgrade", c => c.Short(nullable: false));
            AlterColumn("dbo.ShellEffect", "EffectLevel", c => c.Byte());
            DropColumn("dbo.Account", "ReferrerId");
            DropColumn("dbo.Account", "DailyRewardSent");
            DropColumn("dbo.Character", "GoldBank");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Character", "GoldBank", c => c.Long(nullable: false));
            AddColumn("dbo.Account", "DailyRewardSent", c => c.Boolean(nullable: false));
            AddColumn("dbo.Account", "ReferrerId", c => c.Long(nullable: false));
            DropForeignKey("dbo.LogCommands", "CharacterId", "dbo.Character");
            DropForeignKey("dbo.CharacterVisitedMaps", "CharacterId", "dbo.Character");
            DropForeignKey("dbo.CharacterTitle", "CharacterId", "dbo.Character");
            DropIndex("dbo.LogCommands", new[] { "CharacterId" });
            DropIndex("dbo.CharacterVisitedMaps", new[] { "CharacterId" });
            DropIndex("dbo.CharacterTitle", new[] { "CharacterId" });
            AlterColumn("dbo.ShellEffect", "EffectLevel", c => c.Byte(nullable: false));
            DropColumn("dbo.ShellEffect", "Upgrade");
            DropColumn("dbo.ShellEffect", "Type");
            DropColumn("dbo.ShellEffect", "IsRune");
            DropColumn("dbo.Quest", "CanBeDoneOnlyOnce");
            DropColumn("dbo.ScriptedInstance", "QuestTimeSpaceId");
            DropColumn("dbo.Portal", "RequiredClass");
            DropColumn("dbo.Portal", "NomeOggetto");
            DropColumn("dbo.Portal", "HeroLevelRequired");
            DropColumn("dbo.Portal", "LevelRequired");
            DropColumn("dbo.Portal", "RequiredItem");
            DropColumn("dbo.CharacterSkill", "TattooLevel");
            DropColumn("dbo.CharacterSkill", "IsTattoo");
            DropColumn("dbo.MapNpc", "Score");
            DropColumn("dbo.NpcMonster", "EvolvePet");
            DropColumn("dbo.Item", "MorphSp");
            DropColumn("dbo.ItemInstance", "RuneAmount");
            DropColumn("dbo.ItemInstance", "IsBreaked");
            DropColumn("dbo.ItemInstance", "HasSkin");
            DropColumn("dbo.Character", "AutoLoot");
            DropColumn("dbo.Character", "UltraClass");
            DropColumn("dbo.Character", "UiBlocked");
            DropColumn("dbo.Character", "HideHat");
            DropColumn("dbo.Character", "UnlockedHLevel");
            DropColumn("dbo.Character", "MobKillCounter");
            DropColumn("dbo.Character", "IsChangeName");
            DropColumn("dbo.Character", "ArenaDeath");
            DropColumn("dbo.Character", "ArenaKill");
            DropColumn("dbo.Character", "MaxPartnerCount");
            DropColumn("dbo.Account", "GoldBank");
            DropTable("dbo.RuneEffect");
            DropTable("dbo.LogCommands");
            DropTable("dbo.ChatLog");
            DropTable("dbo.BoxItem");
            DropTable("dbo.BotAuthority");
            DropTable("dbo.CharacterVisitedMaps");
            DropTable("dbo.CharacterTitle");
        }
    }
}
