﻿namespace OpenNos.Data
{
    public class CharacterVisitedMapDTO
    {
        public long CharacterVisitedMapId { get; set; }
        
        public long CharacterId { get; set; }
        
        public int MapId { get; set; }
        
        public int MapX { get; set; }
        
        public int MapY { get; set; }
    }
}