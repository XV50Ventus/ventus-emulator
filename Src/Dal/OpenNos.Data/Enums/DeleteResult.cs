﻿namespace OpenNos.Data.Enums
{
    public enum DeleteResult : byte
    {
        Unknown = 0,
        Deleted = 1,
        Error = 2,
        NotFound = 3
    }
}